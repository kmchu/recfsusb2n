## これ何？
フォーク元:  
地デジTV TunerのFSUSB2N向け  
recfsusb2n ver. 0.9.2 (Linux版)  
http://tri.dw.land.to/fsusb2n/  

以下の地デジチューナでts抜きするためのソフトです  
* KTV-FSUSB2
* KTV-FSPCIE

## 準備
ユーザーをvideoに追加
```bash
$ sudo gpasswd -a ユーザー名 video
```

なんかよくわからんけど何か
```bash
$ sudo echo SUBSYSTEM=="usb", ENV{DEVTYPE}=="usb_device", ATTRS{idVendor}=="0511", ATTRS{idProduct}=="0029", MODE="0664", GROUP="video" >>  /lib/udev/rules.d/89-tuner.rules
```

認識を確認
```bash
$ lsusb -d 0511:
Bus 002 Device 003: ID 0511:0029 N'Able (DataBook) Technologies, Inc.
```

## ビルド

recfsusb2n/src/CMakeLists.txt

FSUSB2内蔵カードリーダ使うばあい

```
    # Libraries
-   SET(FSUSB_LINK_INT "-lpthread")
+   SET(FSUSB_LINK_INT "-lpthread -lboost_system") #追加
    SET(FSUSB_LINK_EXT "${BOOST_LDFLAGS}")
```

FSUSB2内蔵カードリーダ使わないなら
```
    # Libraries
-   SET(FSUSB_LINK_INT "-lpthread")
+   SET(FSUSB_LINK_INT "-lpthread -lboost_system -larib25") #追加
    SET(FSUSB_LINK_EXT "${BOOST_LDFLAGS}")
    # Sources
    FILE(GLOB FSUSB_SRC ${FSUSB_SRC_DIR}/*.cpp)
-   FILE(GLOB ARIB25_SRC ${ARIB25_SRC_DIR}/*.c)
+   #FILE(GLOB ARIB25_SRC ${ARIB25_SRC_DIR}/*.c) #コメントアウト
```
```bash
$ sudo apt-get install libboost-thread-dev libboost-filesystem-dev build-essential pkg-config cmake
$ git clone https://gitlab.com/kmchu/recfsusb2n.git
$ cd recfsusb2n
$ mkdir build
$ cd build
$ cmake ..
$ make
```


## 使い方

makeでbuildした後に生成される`build/src/recfsusb2n`を実行すれば使えます。

```bash
$ ./recfsusb2n --b25 [録画するchannel] [録画秒数] [出力先ファイル名]
```
例:
```bash
$ ./recfsusb2n --b25 -v 27 30 test.ts
```

録画秒数に - を指定するとSIGTERM, Ctrl+C等でinterruptされるまで録画を続けます。
出力先ファイル名に - を指定すると標準出力にTSを出力します。その場合、ログは標準エラー出力に出力されます。

## メモ
chinachu向け
```
- name: FSUSB2
  types:
    - GR
  command: recfsusb2n -b <channel> - -
  isDisabled: false

```



